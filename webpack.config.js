const glob = require('glob')
const path = require('path')
const JsPlugins = require('./webpack/Js')
const CssPlugins = require('./webpack/Css')
const SassPlugins = require('./webpack/Sass')
const CopyPlugins = require('./webpack/Copy')
const CleanPlugins = require('./webpack/Clean')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const pages = glob.sync(__dirname + '/src/pages/*.pug')
const files = pages.map(file => {
  const base = path.basename(file, '.pug')

  return new HtmlWebpackPlugin({
    inject: true,
    filename: './' + base + '.html',
    template: './src/pages/' + base + '.pug'
  })
})

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  output: { filename: './js/bundle.js' },
  entry: ['./src/js/app.js', './src/scss/style.scss'],
  module: {
    rules: [
      {
        test: /\.pug$/,
        use: ['pug-loader']
      },
      {
        test: /\.ext$/,
        include: path.resolve('./src'),
        use: ['cache-loader', 'pug-loader', 'babel-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
      },
      JsPlugins,
      SassPlugins
    ]
  },
  plugins: [CleanPlugins(), CssPlugins(), ...files], //TODO: Insert CopyPlugins() to build
  optimization: {
    runtimeChunk: true
  }
}
